package com.ginius.porfolio.app;

import com.ginius.potfolio.app.PortfolioApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PortfolioApplication.class)
public class PortfolioApplicationTests {

    @Test
    public void contextLoads() {
    }

}
